<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>

<table border="1">

	<tr>
		<th>FIRSTNAME</th>
		<th>LASTNAME</th>
		<th>ACTION</th>
	</tr>
	
<c:forEach items="${searchList}" var="i">
	<tr>
		<td>${i.firstName}</td>
		<td>${i.lastName}</td>
		
		<td>
			<a href="deleteData?id=${i.id}">DELETE</a>
			<a href="editData?id=${i.id}">EDIT</a>
		</td>
		
	</tr>
</c:forEach>

</table>
</body>
</html>