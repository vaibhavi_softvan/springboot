package com.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.demo.model.RegisterVO;
import com.demo.service.RegisterService;
import com.demo.utils.Pages;

@Controller
public class RegisterController {

	@Autowired
	RegisterService registerService;

	private static final String REGISTERVOKEY = "RegisterVO";

	@GetMapping(value = "/")
	public String load(ModelMap model) {

		model.addAttribute(REGISTERVOKEY, new RegisterVO());
		return Pages.REGISTER.getValue();
	}

	@PostMapping(value = "/insertData")
	public String insertData(@ModelAttribute RegisterVO registerVO) {

		this.registerService.insert(registerVO);
		return "redirect:/";
	}

	@GetMapping(value = "/searchData")
	public String searchData(ModelMap model) {

		@SuppressWarnings("unchecked")
		List<RegisterVO> searchList = (List<RegisterVO>) this.registerService.commenQuery("", REGISTERVOKEY);

		model.addAttribute("searchList", searchList);
		return Pages.SEARCH.getValue();
	}

	@GetMapping(value = "/deleteData")
	public String deleteData(RegisterVO registerVO, @RequestParam Long id) {

		registerVO.setId(id);
		this.registerService.delete(registerVO);

		return "redirect:/searchData";
	}

	@GetMapping(value = "/editData")
	public String editData(@RequestParam Long id, ModelMap model) {

		@SuppressWarnings("unchecked")
		List<RegisterVO> registerList = (List<RegisterVO>) this.registerService.commenQuery("",
				"RegisterVO where id = " + id);

		model.addAttribute(REGISTERVOKEY, registerList.get(0));
		return Pages.REGISTER.getValue();
	}

}
