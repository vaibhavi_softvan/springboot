package com.demo.utils;

public enum Pages {

	REGISTER("/Register"), SEARCH("/ViewRegister");

	private final String value;

	public String getValue() {
		return value;
	}

	private Pages(String value) {
		this.value = value;
	}

}
