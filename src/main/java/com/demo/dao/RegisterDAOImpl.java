package com.demo.dao;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RegisterDAOImpl implements RegisterDAO {

	@Autowired
	private SessionFactory sessionFactory;

	private static final Log log = LogFactory.getLog(RegisterDAOImpl.class);
	private static final String ERROR = "Error : ";
	private static final String FROM = " from ";

	public Boolean insert(Object object) {
		try {
			Session session = sessionFactory.getCurrentSession();
			session.saveOrUpdate(object);
			return true;

		} catch (Exception e) {
			log.error(ERROR, e);
			return false;
		}
	}

	@SuppressWarnings("rawtypes")
	public List<?> commenQuery(String sufix, String prefix) {
		try {
			String query = sufix + FROM + prefix;
			Session session = sessionFactory.getCurrentSession();
			log.info("Query : " + query);
			Query sqlQuery = session.createQuery(query);
			return sqlQuery.list();
		} catch (HibernateException e) {
			log.info(ERROR, e);
			return new ArrayList();
		}

	}

	public Boolean delete(Object object) {
		try {
			Session session = sessionFactory.getCurrentSession();
			session.delete(object);
			return true;

		} catch (Exception e) {
			log.error(ERROR, e);
			return false;
		}
	}

}
