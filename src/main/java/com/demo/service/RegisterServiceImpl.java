package com.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.dao.RegisterDAO;

@Service
public class RegisterServiceImpl implements RegisterService {

	@Autowired
	RegisterDAO registerDAO;

	@Override
	@Transactional
	public Boolean insert(Object object) {
		return this.registerDAO.insert(object);
	}

	@Override
	@Transactional
	public List<?> commenQuery(String suffix, String prefix) {
		return this.registerDAO.commenQuery(suffix, prefix);
	}

	@Override
	@Transactional
	public Boolean delete(Object object) {
		return this.registerDAO.delete(object);
	}

}